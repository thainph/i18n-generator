# I18n Generator

I18n Generator for Laravel.

# Installation

1. Config in app.php ` Thainph\I18nGenerator\I18nGeneratorServiceProvider,`

# How to use
Run below command to generate all localization strings to json:
```
php artisan i18n:generate {path-to-directory}
```
Import localization strings to i18n
```
import en from '../lang/en.json';
import ja from '../lang/en.json';
import Vue from 'vue';
import VueI18n from 'vue-i18n';

Vue.use(VueI18n);

const messages = {
    en,
    ja,
}

const i18n = new VueI18n({
    locale: document.head.querySelector('meta[name="locale"]').content,
    messages: messages
});

export default i18n;

```
