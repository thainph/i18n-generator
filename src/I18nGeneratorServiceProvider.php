<?php
namespace Thainph\I18nGenerator;

use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Http\Kernel;
use Thainph\I18nGenerator\Console\GenerateI18n;

class I18nGeneratorServiceProvider extends ServiceProvider
{
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                GenerateI18n::class,
            ]);
        }

    }
}
